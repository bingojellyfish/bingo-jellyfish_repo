Feature: Home Page
  In order to test Home Page of application
  As a Registered user
  I want to specify the features of home page

  Scenario: Home Page Default content
    Given a registered user exists
    Given user is on Tasks login page
    When user enters username
    And user enters password
    And user clicks on login button
    Then user is on Application home page
    And user gets a ToDoList section

  Scenario: ToDoList Section
    Given user is on Tasks loginpage
    When user enters username
    And user enters password
    And user clicks on login button
    Then user is on Application home page
    When user focuses...
 