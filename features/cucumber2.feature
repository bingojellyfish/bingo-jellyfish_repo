Feature: Loging in to web application to organizing tasks

  Background:
    Given there is a user who wants to organize tasks
    Given the user has a Google account
    When the user logs in with the Google Authentication

    Scenario: User should be able to create a new task
    Given the user is logged in successfully with a Google account
    Then the user can view a message "Successfully authenticated from Google account."
    When the user clicks "New Task"
    Then the user should see two input boxes and a checkbox
    When the user fills in Todo box with "Cucumber Test"
    And the user fills in Priority box with "1"
    And the user checks the Completed checkbox
    And the user clicks "Create Task"
    Then a message "Task was successfully created" shows


  Scenario: User should be able to Edit tasks
    Given the user created a new task
    Then the user should be able to access Tasks page
    When the user clicks on "Edit"
    Then the user should see the Editing Task page 
    When the user fills in Todo box with "Cucumber Test"
    And the user fills in Priority box with "1"
    And the user does not check the Completed checkbox
    And the user clicks "Update Task"
    Then a message "Task was successfully updated" shows 

  Scenario: User should be able to Destroy tasks
    Given the user created a new task
    Then the user should be able to access Tasks page
    When the user clicks on "Destroy"
    Then the message "Are you sure?" pops up
    When the user clicks "OK"
    Then the message "Task was successfully destoryed" shows
    And the task is deleted on the Todo list