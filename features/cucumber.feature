Feature: Loging in to web application to organizing tasks

  Background:
    Given there is a user who wants to organize tasks
    When the user accesses the web application page for organizing tasks
 

  Scenario: User should be able to log in with Google Authentication
    Given a user is on login page
    When the user clicks "Sign in with GoogleOauth2"
    And the user clicks "Use another Account"
    And the user fills in "Email" with "solji.cho@stonybrook.edu"
    And the user clicks "Next"
    And the user fills in "Password" with "bingo1234"
    Then the user can view a message "Successfully authenticated from Google account"

  Scenario: User should not be able to log in with wrong password
    Given the user is on login page
    And the user fills in "Email" with "yerin.cho@stonybrook.edu"
    When the user fills in "Password" with "bingo"
    And the clicks "Log in"
    Then a message "Invalid Email or password." shows

 Scenario: User with no account should be able to Sign up
  Given a user does not have account
  When the user clicks "Sign up"
  Then Sign up page shows up with three blank fields
  When the user fills in "Email" with "jimin.oh@stonybrook.edu"
  And the user fills in "Password" with "bingoj"
  And the user fills in "Password confirmation" with "bingoj"
  And the user clicks "Sign up"
  Then the message "Welcome! You have signed up successfully." shows  

Scenario: User can not sign up if he/she fills in "Password" with less than 6 characters 
  Given a user does not have account
  When the user clicks "Sign up"
  Then Sign up page shows up with three blank boxes
  When the user fills in "Email" with "jimin.oh@stonybrook.edu"
  And the user fills in "Password" with "bingo"
  And the user fills in "Password confirmation" with "bingo"
  And the user clicks "Sign up"
  Then the error message "1 error prohibited this user from being saved: Password is too short(minimum is 6 characters)" shows

  Scenario: User can sign in if he/she forgot the password 
  Given a user forgot the password
  When the user clicks "Forgot the password?"
  Then the user can input Email in Forgot your password? page
  When the user fills in "Email" with "solji.choi@stonybrook.edu"
  And the user clicks "Send me reset password instructions"
  And the user is able to reset the new password
  Then the user can sign in with the new password